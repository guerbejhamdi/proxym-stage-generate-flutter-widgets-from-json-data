import 'package:flutter/material.dart';
import 'package:form_generator_proxym/Forms/register_form.dart';

 
void main() => runApp(MyApp());
 
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Generated From'),
        ),
        body: RegisterForm(),
      ),
    );
  }
}