import 'package:flutter/material.dart';
import 'package:form_generator_proxym/Model/BaseWidget.dart';
import 'package:form_generator_proxym/Utils/base_urls.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'create_listView.dart';


class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  Future<String> futurejsonFromServer;

  String jsonFromServer;
  Future<bool> fetchedJson;
  Future<String> fetchServer() async {
    http.Response response =
        await http.get(Uri.parse(BaseUrls.jsonFromServerUrl));
    jsonFromServer = json.decode(response.body);

    return jsonFromServer;
  }

  @override
  void initState() {
    super.initState();
  }

  Future<Basewidget> fetchData() async {
    http.Response response = await http.get(BaseUrls.jsonFromServerUrl);
    String data = response.body;

    //
    String jsonString = data;

    try {
      Basewidget basewidget = basewidgetFromJson(jsonString);
      return basewidget;
    } catch (e) {
      print(e);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
      future: fetchData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Center(child: new Text('Trying to load data from server...'));
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return CreateListView(context: context, snapshot: snapshot);
        }
      },
    );

    return new Scaffold(
      body: futureBuilder,
    );
  }
}


