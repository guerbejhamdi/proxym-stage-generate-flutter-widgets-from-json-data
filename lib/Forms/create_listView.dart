import 'package:flutter/cupertino.dart';
import 'package:form_generator_proxym/CustomWidgets/custom_form.dart';
import 'package:form_generator_proxym/Model/BaseWidget.dart';

class CreateListView extends StatelessWidget {
  const CreateListView({
    Key key,
    @required this.context,
    @required this.snapshot,
  }) : super(key: key);

  final BuildContext context;
  final AsyncSnapshot snapshot;

  @override
  Widget build(BuildContext context) {
    List<Mywidget> widgetList;
    Basewidget values = snapshot.data;
    widgetList = values.mywidgets;
 
    return Customform(widgetList);

  }
}