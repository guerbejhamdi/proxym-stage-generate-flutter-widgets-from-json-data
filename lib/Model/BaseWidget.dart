// To parse this JSON data, do
//
//     final basewidget = basewidgetFromJson(jsonString);

import 'dart:convert';

Basewidget basewidgetFromJson(String str) => Basewidget.fromJson(json.decode(str));

String basewidgetToJson(Basewidget data) => json.encode(data.toJson());

class Basewidget {
    Basewidget({
        this.mywidgets,
    });

    List<Mywidget> mywidgets;

    factory Basewidget.fromJson(Map<String, dynamic> json) => Basewidget(
        mywidgets: List<Mywidget>.from(json["mywidgets"].map((x) => Mywidget.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mywidgets": List<dynamic>.from(mywidgets.map((x) => x.toJson())),
    };
}

class Mywidget {
    Mywidget({
        this.key,
        this.type,
        this.label,
        this.placeholder,
        this.required,
        this.keyboardType,
        this.controller,
        this.validator,
        this.obscureText,
    });

    String key;
    String type;
    String label;
    String placeholder;
    bool required;
    String keyboardType;
    String controller;
    Validator validator;
    bool obscureText;

    factory Mywidget.fromJson(Map<String, dynamic> json) => Mywidget(
        key: json["key"],
        type: json["type"],
        label: json["label"],
        placeholder: json["placeholder"] == null ? null : json["placeholder"],
        required: json["required"],
        keyboardType: json["keyboardType"],
        controller: json["controller"],
        validator: Validator.fromJson(json["validator"]),
        obscureText: json["obscureText"] == null ? null : json["obscureText"],
    );

    Map<String, dynamic> toJson() => {
        "key": key,
        "type": type,
        "label": label,
        "placeholder": placeholder == null ? null : placeholder,
        "required": required,
        "keyboardType": keyboardType,
        "controller": controller,
        "validator": validator.toJson(),
        "obscureText": obscureText == null ? null : obscureText,
    };
}

class Validator {
    Validator({
        this.value,
        this.isEmpty,
        this.length,
        this.isEmail,
    });

    String value;
    bool isEmpty;
    int length;
    bool isEmail;

    factory Validator.fromJson(Map<String, dynamic> json) => Validator(
        value: json["value"],
        isEmpty: json["isEmpty"],
        length: json["length"],
        isEmail: json["isEmail"] == null ? null : json["isEmail"],
    );

    Map<String, dynamic> toJson() => {
        "value": value,
        "isEmpty": isEmpty,
        "length": length,
        "isEmail": isEmail == null ? null : isEmail,
    };
}
