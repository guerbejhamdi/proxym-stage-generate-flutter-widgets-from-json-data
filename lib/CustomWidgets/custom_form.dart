import 'package:flutter/material.dart';
import 'package:form_generator_proxym/CustomWidgets/custom_generatedWidget.dart';
import 'package:form_generator_proxym/CustomWidgets/generate_checkbox.dart';
import 'package:form_generator_proxym/Model/BaseWidget.dart';

class Customform extends StatefulWidget {
  List<Mywidget> widgetList;
  String test;

  Customform(this.widgetList);
  @override
  _CustomformState createState() => _CustomformState();
}

class _CustomformState extends State<Customform> {
  final GlobalKey<FormState> _globalKey = new GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Number of widgets : " + this.widget.widgetList.length.toString());

    Widget generateWidgetFromType(Mywidget mywidget) {
      switch (mywidget.type) {
        case "CheckBox":
          return GenerateCheckBox(mywidget);
        case "Input":
          return CustomGeneratedWidget(mywidget);
        case "DatePicker":
          return CustomGeneratedWidget(mywidget);
      }
      return Text("This type is not supported yet");
    }

    return SingleChildScrollView(
      child: Form(
        key: _globalKey,
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Text("Register Form: "),
            SizedBox(
              height: 3,
            ),
            for (var i = 0; i < this.widget.widgetList.length; i++)
              generateWidgetFromType(this.widget.widgetList[i]),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text("Add"),
                  onPressed: () {
                    if (_globalKey.currentState.validate()) {
                      _globalKey.currentState.save();
                     for (var i = 0; i < this.widget.widgetList.length; i++)
                        print("Data:"+CustomGeneratedWidget(this.widget.widgetList[i]).getValue());

                      // var titleTextValue = store.get(this.widget.widgetList[i].controller.toString());

                      //print('title is ' + titleTextValue());

                      //print("this"+widget.test);

                    }
                  },
                ),
                SizedBox(width: 10),
                ElevatedButton(
                  child: Text("clear"),
                  onPressed: () {
                    _globalKey.currentState.reset();
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
