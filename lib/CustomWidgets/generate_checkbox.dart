import 'package:flutter/material.dart';
import 'package:form_generator_proxym/Model/BaseWidget.dart';

class GenerateCheckBox extends StatefulWidget {
  Mywidget mywidget;
  GenerateCheckBox(this.mywidget);
  @override
  _GenerateCheckBoxState createState() => _GenerateCheckBoxState();
}

class _GenerateCheckBoxState extends State<GenerateCheckBox> {
  bool valuefirst = false;  
  bool valuesecond = false;  

  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Text(this.widget.mywidget.label),
        Checkbox(
              checkColor: Colors.greenAccent,
              activeColor: Colors.red,
              value: this.valuefirst,
              onChanged: (bool value) {
                setState(() {
                  this.valuefirst = value;
                });
              },
            ),
      ],
    );
  }
}
