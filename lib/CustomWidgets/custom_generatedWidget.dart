import 'package:flutter/material.dart';
import 'package:form_generator_proxym/Model/BaseWidget.dart';
import 'package:validators/validators.dart';

class CustomGeneratedWidget extends StatefulWidget {
  //final FormFieldSetter<String> onSaved;

  final TextEditingController titleController = TextEditingController();

  Mywidget mywidget;
  CustomGeneratedWidget(this.mywidget);
  String getValue() {
    return titleController.text;
  }

  bool validateStructure(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  TextInputType customType(String key) {
    if (key.contains("number")) {
      return TextInputType.number;
    } else if (key.contains("password")) {
      return TextInputType.visiblePassword;
    }
    return TextInputType.name;
  }

  bool isPasswordField(String key) {
    if (key.contains("password")) {
      return true;
    } else {
      return false;
    }
  }

  Widget _buildChild() {
    if (this.mywidget.type.contains("Input")) {
      return Container(
        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: TextFormField(
          controller: titleController,
          obscureText: isPasswordField(this.mywidget.key),
          keyboardType: customType(this.mywidget.key),
          decoration: InputDecoration(
              labelText: this.mywidget.label, border: OutlineInputBorder()),
          validator: (value) {
            //Checking length
            if (this.mywidget.validator.length != null) {
              if (value.length < this.mywidget.validator.length) {
                return "Must contain " +
                    this.mywidget.validator.length.toString() +
                    " characters";
              }
            }
            //Checking if the input is an email
            if (this.mywidget.validator.isEmail == true) {
              if (!isEmail(value)) {
                return "Invalid Email";
              }
            }
            //Checking if the input is an a number
            if (this.mywidget.key.contains("number")) {
              if (int.tryParse(value) == null) {
                return "Must be a number";
              }
            }

            //Checking if the input is an a password
            if (this.mywidget.key.contains("password")) {
              if (!validateStructure(value)) {
                return "Password must contain number and a special character!";
              }
            }

            if (value.isEmpty) {
              return "Required";
            }
            return null;
          },
          // onSaved: widget.onSaved,
        ),
      );
    } else {
      TextEditingController dateCtl = TextEditingController();
      BuildContext context;
      return Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: TextFormField(
          controller: dateCtl,
          decoration: InputDecoration(
            labelText: "Date of birth",
            hintText: "Ex. Insert your dob",
          ),
          onTap: () async {
            DateTime date = DateTime(1900);
            FocusScope.of(context).requestFocus(new FocusNode());

            date = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1900),
                lastDate: DateTime(2100));
            dateCtl.text = date.toIso8601String();
          },
        ),
      );
    }
  }

  @override
  _CustomGeneratedWidgetState createState() => _CustomGeneratedWidgetState();
}

class _CustomGeneratedWidgetState extends State<CustomGeneratedWidget> {
  bool validateStructure(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  TextInputType customType(String key) {
    if (key.contains("number")) {
      return TextInputType.number;
    } else if (key.contains("password")) {
      return TextInputType.visiblePassword;
    }
    return TextInputType.name;
  }

  bool isPasswordField(String key) {
    if (key.contains("password")) {
      return true;
    } else {
      return false;
    }
  }

  final TextEditingController titleController = TextEditingController();

  String getValue() {
    return titleController.text;
  }

  titleTextValue(TextEditingController myController) {
    print("title text field: ${myController.text}");
    return myController.text;
  }

  @override
  Widget build(BuildContext context) {
    Map<String, TextEditingController> textEditingControllers = {};
    var textEditingController = new TextEditingController(
        text: this.widget.mywidget.controller.toString());

    textEditingControllers.putIfAbsent(
        this.widget.mywidget.controller, () => textEditingController);

    store.set(this.widget.mywidget.controller.toString(),
        titleTextValue(textEditingController));

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: this.widget._buildChild(),
    );
  }
}

class GlobalState {
  final Map<dynamic, dynamic> _data = <dynamic, dynamic>{};

  static GlobalState instance = GlobalState._();
  GlobalState._();

  set(dynamic key, dynamic value) => _data[key] = value;
  get(dynamic key) => _data[key];
}

final GlobalState store = GlobalState.instance;
